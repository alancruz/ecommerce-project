from django.urls import path
from django.conf.urls.static import static
from . import views

app_name = "search_app"

urlpatterns = [
    path('', views.searchResult, name='searchResult'),
]
